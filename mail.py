# Python 3.8.0
import smtplib
import time
import imaplib
import email
import traceback 
from pprint import pprint
import json
# -------------------------------------------------
#
# Utility to read email from Gmail Using Python
#
# ------------------------------------------------
ORG_EMAIL = "@gmail.com" 
FROM_EMAIL = "Your-ID" + ORG_EMAIL 
FROM_PWD = "Password" 
SMTP_SERVER = "imap.gmail.com" 
SMTP_PORT = 993
mail_list = {}

def read_email_from_gmail():
    try:
        mail = imaplib.IMAP4_SSL(SMTP_SERVER)
        mail.login(FROM_EMAIL,FROM_PWD)
        mail.select('inbox')

        data = mail.search(None, 'ALL')
        mail_ids = data[1]
        id_list = mail_ids[0].split()   
        first_email_id = int(id_list[0])
        latest_email_id = int(id_list[-1])

        for i in range(latest_email_id,first_email_id, -1):
            data = mail.fetch(str(i), '(RFC822)' )
            for response_part in data:
                arr = response_part[0]
                if isinstance(arr, tuple):
                    msg = email.message_from_string(str(arr[1],'utf-8'))
                    # Email header keys
                    # ['Delivered-To', 'Received', 'X-Received', 'ARC-Seal', 'ARC-Message-Signature', 'ARC-Authentication-Results', 'Return-Path', 'Received', 'Received-SPF', 'Authentication-Results', 'DKIM-Signature', 'X-Google-DKIM-Signature', 'X-Gm-Message-State', 'X-Google-Smtp-Source', 'MIME-Version', 'X-Received', 'Date', 'X-Account-Notification-Type', 'Feedback-ID', 'X-Notifications', 'X-Notifications-Bounce-Info', 'Message-ID', 'Subject', 'From', 'To', 'Content-Type']
                    # print(list(msg.keys()))
                    
                    
                    email_subject = msg['subject']
                    email_from = msg['from']
                    email_Message_ID = msg['Message-ID']
                    singleMail = {'From': email_from, 'Subject': email_subject, 'Message_ID':email_Message_ID }
                    mail_list[i] = singleMail

    except Exception as e:
        traceback.print_exc() 
        print(str(e))

read_email_from_gmail()
# convert this list to JSON format
# json_string = json.dumps(mail_list)
# print(json_string)
pprint(mail_list)